require('dotenv').config();
// const path = require('path');
const env = process.env.NODE_ENV;

//Start-Development Environment Config
const prod = {};
const dev = {
  app: {
    port: parseInt(process.env.DEV_APP_PORT)
  },
  db: {
    host: process.env.DEV_DB_HOST,
    port: parseInt(process.env.DEV_DB_PORT),
    database: process.env.DEV_DB_NAME,
    username: process.env.DEV_DB_USER_NAME,
    password: process.env.DEV_DB_PASSWORD,
    dialect: process.env.DEV_DB_DIALECT,
    dialectOptions: {
      "bigNumberStrings": true
    }
  },
  jwt: {
    jwtApiSecret: process.env.DEV_JWT_AUTHKEY
  },
  sendgrid: {
    sendgridemail: process.env.DEV_SENDGRID_EMAIL,
    sendgridApiKey: process.env.DEV_SENDGRID_API_KEY,
    senderName: process.env.DEV_SENDER_NAME
  },
  twilio: {
    accountId: process.env.DEV_TWILIO_ACCOUNTID,
    authToken: process.env.DEV_TWILIO_AUTHTOKEN,
    smsFrom: process.env.DEV_SMS_FROM
  },
  opentok: {
    apikey: process.env.DEV_OPENTOK_API_KEY,
    apisecret: process.env.DEV_OPENTOK_API_SECRET,
  },
  stripe: {
    secretkey: process.env.DEV_STRIPE_SECRETKEY,
    defaultCharge: process.env.DEV_STRIPE_DEFAULT_CHARGE
  },
  aws: {
    bucketAccessKey: process.env.DEV_AWS_ACCESS_KEY,
    bucketSecretKey: process.env.DEV_AWS_SECRET_KEY,
    bucketName: process.env.DEV_AWS_BUCKET_NAME
  },
  agora: {
    agoraAppId: process.env.DEV_AGORA_APP_ID,
    agoraSecretId: process.env.DEV_AGORA_SECRET_ID,
  }
};
//End-Development Environment Config
const config = {
  prod,
  dev
};
module.exports = config[env];
console.log('config.js:- End.');
